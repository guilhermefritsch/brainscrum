<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<h1>Novo Cadastro</h1>

		<form method="post" action="cadastro/insert" >
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<label for="nome">* Nome</label>
						<input type="text" id="nome" name="name" placeholder="Nome completo" class="form-control" value="<?= isset($name) ? $name : '' ?>" />
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<label for="email">* Email</label>
						<input type="text" id="email" name="email" placeholder="Email" class="form-control" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<label for="apelido">* Apelido</label>
						<input type="text" id="apelido" name="nick" placeholder="Nome completo" class="form-control" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<label for="senha">* Escolha uma senha</label>
						<input type="password" id="senha" name="password" placeholder="Senha" class="form-control" />
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<label for="re-senha">* Repita a senha</label>
						<input type="password" id="re-senha" name="confirm" placeholder="Repita a senha" class="form-control"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-md btn-success">Cadastrar</button>
				</div>
			</div>
		</form>
	</div>
</div>