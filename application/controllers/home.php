<?php
	class Home extends CI_Controller {

		public function __construct()
    {
            parent::__construct();
            //$this->load->model('news_model');
    }

		public function index(){
			$data['titulo'] = 'Home';
			$this->load->view('inc/header', $data);
      $this->load->view('inc/menu_principal');
      $this->load->view('home/index');
      $this->load->view('inc/footer');
    }

	}
?>